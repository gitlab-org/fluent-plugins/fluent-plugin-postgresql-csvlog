# frozen_string_literal: true

require_relative '../helper'

class PgStatStatementsInputIntegrationTest < Test::Unit::TestCase
  # The defaults values work with the configuration in .gitlab-ci.yml on the postgres service
  # Override with env vars for local development
  HOST = ENV.fetch('PG_TEST_HOST', 'postgres')
  USERNAME = ENV.fetch('PG_TEST_USER', 'testuser')
  PASSWORD = ENV.fetch('PG_TEST_PASSWORD', 'testpass')

  def setup
    Fluent::Test.setup

    @conn = PG.connect(
      host: HOST,
      user: USERNAME,
      password: PASSWORD
    )

    try_setup_extension
    create_known_statement
  end

  def teardown
    @conn&.finish
  end

  # Setup pg_stat_statements extension
  def try_setup_extension
    @conn.exec('CREATE EXTENSION pg_stat_statements')
  rescue PG::DuplicateObject
  end

  # This statement gives us something to look for in the emitted stream
  def create_known_statement
    @conn.exec('SELECT * FROM pg_stat_statements ORDER BY queryid LIMIT 1')
  end

  VALID_CONFIG = %(
    tag postgres.pg_stat_statements
    host #{HOST}
    username #{USERNAME}
    password #{PASSWORD}
    interval 1
  )

  INVALID_CONFIG = %(
    host 'invalid_host.dne'
    port 1234
    username #{USERNAME}
    password #{PASSWORD}
    interval 1
  )

  def create_driver(config)
    Fluent::Test::InputTestDriver.new(Fluent::Plugin::PgStatStatementsInput).configure(config)
  end

  sub_test_case 'configuration' do
    test 'connects' do
      d = create_driver(VALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      assert_false emits.empty?
    end

    # Why do we have this test? If postgres is still starting up, we don't want to cause the
    # the fluentd configuration to fail. We would rather retry until we get a connection
    test 'connects for an invalid config' do
      d = create_driver(INVALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      assert_true emits.empty?
    end
  end

  sub_test_case 'execution' do
    test 'connects' do
      d = create_driver(VALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      expected_record = {
        'fingerprint' => '8a6e9896bd9048a2',
        'query' => 'SELECT * FROM pg_stat_statements ORDER BY queryid LIMIT $1',
        'query_length' => 58
      }
      known_statement_event = emits.find do |event|
        record = event[2]
        record['query'] == expected_record['query']
      end

      assert_false known_statement_event.nil?

      tag = known_statement_event[0]
      record = known_statement_event[2]

      assert_equal 'postgres.pg_stat_statements', tag
      assert_equal expected_record['fingerprint'], record['fingerprint']
      assert_equal expected_record['query_length'], record['query_length']
      assert_true record.include? 'queryid'
      assert_true record['queryid'].is_a? String
    end
  end
end
