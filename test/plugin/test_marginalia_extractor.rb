# frozen_string_literal: true

require_relative '../helper'

class Marginalia < Test::Unit::TestCase
  include Fluent::Plugin::MarginaliaExtractor

  def test_parse(sql, record, key, strip_comment, expected)
    record[key] = sql
    parse_marginalia_into_record(record, key, strip_comment)
    assert_equal(expected, record)
  end

  test 'no marginalia' do
    sql = 'SELECT * FROM projects'
    expected = { 'sql' => 'SELECT * FROM projects' }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'normal comment appended' do
    sql = 'SELECT COUNT(*) FROM "projects" /* this is just a comment */'
    expected = {
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia appended for sidekiq' do
    sql = 'SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/'
    expected = {
      'application' => 'sidekiq',
      'correlation_id' => 'd67cae54c169e0cab7d73389e2934f0e',
      'jid' => '52a1c8a9e4c555ea573f20f0',
      'job_class' => 'Geo::MetricsUpdateWorker',
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia appended for web' do
    sql = 'SELECT COUNT(*) FROM "projects" /*application:web,correlation_id:01F1D2T1SC9DM82A4865ATG1CP,endpoint_id:POST /api/:version/groups/:id/-/packages/mavenpath/:file_name*/'
    expected = {
      'application' => 'web',
      'correlation_id' => '01F1D2T1SC9DM82A4865ATG1CP',
      'endpoint_id' => 'POST /api/:version/groups/:id/-/packages/mavenpath/:file_name',
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia appended with other comments in SQL' do
    sql = %(INSERT INTO "p_ci_builds_metadata" ("project_id", "config_options", "config_variables", "has_exposed_artifacts", "build_id", "partition_id") VALUES (40163635, '{"image":{"name":"$AWS_AMAZONLINUX_IMAGE"},"artifacts":{"paths":["${ASSET_NAME}/out"]},"before_script":["yum install -y python3 python3-pip zip findutils","cd ${ASSET_NAME}"],"script":["mkdir -p venv","cd venv","python3 -m venv pyspark_venvsource","source pyspark_venvsource/bin/activate","pip3 install -r ../scripts/requirements.txt","pip3 install venv-pack","venv-pack -f -o pyspark_venv.tar.gz","cd ..; mkdir -p out/lib","cd src/step/; cp -r --parents ./*/*main*.py ../../out"],"retry":{"max":2}}', '[{"key":"ASSET_NAME","value":"data-pipeline"}]', FALSE, 6269937106, 101) RETURNING "id" /*application:sidekiq,correlation_id:01HQNQAHYZF4Q294P59R7RP71H,jid:047ecd6920dc27ccd49b0aef,endpoint_id:PostReceive,db_config_name:ci*/)
    expected = {
      'application' => 'sidekiq',
      'correlation_id' => '01HQNQAHYZF4Q294P59R7RP71H',
      'jid' => '047ecd6920dc27ccd49b0aef',
      'db_config_name' => 'ci',
      'endpoint_id' => 'PostReceive',
      'sql' => %(INSERT INTO "p_ci_builds_metadata" ("project_id", "config_options", "config_variables", "has_exposed_artifacts", "build_id", "partition_id") VALUES (40163635, '{"image":{"name":"$AWS_AMAZONLINUX_IMAGE"},"artifacts":{"paths":["${ASSET_NAME}/out"]},"before_script":["yum install -y python3 python3-pip zip findutils","cd ${ASSET_NAME}"],"script":["mkdir -p venv","cd venv","python3 -m venv pyspark_venvsource","source pyspark_venvsource/bin/activate","pip3 install -r ../scripts/requirements.txt","pip3 install venv-pack","venv-pack -f -o pyspark_venv.tar.gz","cd ..; mkdir -p out/lib","cd src/step/; cp -r --parents ./*/*main*.py ../../out"],"retry":{"max":2}}', '[{"key":"ASSET_NAME","value":"data-pipeline"}]', FALSE, 6269937106, 101) RETURNING "id")
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'normal comment prepended' do
    sql = '/* this is just a comment */ SELECT COUNT(*) FROM "projects"'
    expected = {
      "sql" => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'normal comment appended with trailing semicolon' do
    sql = 'SELECT COUNT(*) FROM "projects" /* this is just a comment */ ; '
    expected = {
      "sql" => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia prepended for sidekiq' do
    sql = '/*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/ SELECT COUNT(*) FROM "projects"'
    expected = {
      'application' => 'sidekiq',
      'correlation_id' => 'd67cae54c169e0cab7d73389e2934f0e',
      'jid' => '52a1c8a9e4c555ea573f20f0',
      'job_class' => 'Geo::MetricsUpdateWorker',
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia prepended for web' do
    sql = '/*application:web,correlation_id:01F1D2T1SC9DM82A4865ATG1CP,endpoint_id:POST /api/:version/groups/:id/-/packages/mavenpath/:file_name*/ SELECT COUNT(*) FROM "projects"'
    expected = {
      'application' => 'web',
      'correlation_id' => '01F1D2T1SC9DM82A4865ATG1CP',
      'endpoint_id' => 'POST /api/:version/groups/:id/-/packages/mavenpath/:file_name',
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia prepended with other comments in SQL' do
    sql = %(/*application:sidekiq,correlation_id:01HQNQAHYZF4Q294P59R7RP71H,jid:047ecd6920dc27ccd49b0aef,endpoint_id:PostReceive,db_config_name:ci*/ INSERT INTO "p_ci_builds_metadata" ("project_id", "config_options", "config_variables", "has_exposed_artifacts", "build_id", "partition_id") VALUES (40163635, '{"image":{"name":"$AWS_AMAZONLINUX_IMAGE"},"artifacts":{"paths":["${ASSET_NAME}/out"]},"before_script":["yum install -y python3 python3-pip zip findutils","cd ${ASSET_NAME}"],"script":["mkdir -p venv","cd venv","python3 -m venv pyspark_venvsource","source pyspark_venvsource/bin/activate","pip3 install -r ../scripts/requirements.txt","pip3 install venv-pack","venv-pack -f -o pyspark_venv.tar.gz","cd ..; mkdir -p out/lib","cd src/step/; cp -r --parents ./*/*main*.py ../../out"],"retry":{"max":2}}', '[{"key":"ASSET_NAME","value":"data-pipeline"}]', FALSE, 6269937106, 101) RETURNING "id")
    expected = {
      'application' => 'sidekiq',
      'correlation_id' => '01HQNQAHYZF4Q294P59R7RP71H',
      'jid' => '047ecd6920dc27ccd49b0aef',
      'db_config_name' => 'ci',
      'endpoint_id' => 'PostReceive',
      'sql' => %(INSERT INTO "p_ci_builds_metadata" ("project_id", "config_options", "config_variables", "has_exposed_artifacts", "build_id", "partition_id") VALUES (40163635, '{"image":{"name":"$AWS_AMAZONLINUX_IMAGE"},"artifacts":{"paths":["${ASSET_NAME}/out"]},"before_script":["yum install -y python3 python3-pip zip findutils","cd ${ASSET_NAME}"],"script":["mkdir -p venv","cd venv","python3 -m venv pyspark_venvsource","source pyspark_venvsource/bin/activate","pip3 install -r ../scripts/requirements.txt","pip3 install venv-pack","venv-pack -f -o pyspark_venv.tar.gz","cd ..; mkdir -p out/lib","cd src/step/; cp -r --parents ./*/*main*.py ../../out"],"retry":{"max":2}}', '[{"key":"ASSET_NAME","value":"data-pipeline"}]', FALSE, 6269937106, 101) RETURNING "id")
    }
    test_parse(sql, {}, 'sql', true, expected)
  end

  test 'marginalia prepended for web, comment_strip disabled' do
    sql = 'SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/'
    expected = {
      'application' => 'sidekiq',
      'correlation_id' => 'd67cae54c169e0cab7d73389e2934f0e',
      'jid' => '52a1c8a9e4c555ea573f20f0',
      'job_class' => 'Geo::MetricsUpdateWorker',
      'sql' => sql
    }
    test_parse(sql, { 'sql' => sql }, 'sql', false, expected)
  end

  test 'avoid clash' do
    sql = '/*clash_key:bbb*/ SELECT COUNT(*) FROM "projects"'
    expected = {
      'clash_key' => 'aaa',
      'sql_clash_key' => 'bbb',
      'sql' => 'SELECT COUNT(*) FROM "projects"'
    }
    test_parse(sql, { 'clash_key' => 'aaa' }, 'sql', true, expected)
  end
end
