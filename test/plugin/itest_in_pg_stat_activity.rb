# frozen_string_literal: true

require_relative '../helper'

class PgStatActivityInputIntegrationTest < Test::Unit::TestCase
  # The defaults values work with the configuration in .gitlab-ci.yml on the postgres service
  # Override with env vars for local development
  HOST = ENV.fetch('PG_TEST_HOST', 'postgres')
  USERNAME = ENV.fetch('PG_TEST_USER', 'testuser')
  PASSWORD = ENV.fetch('PG_TEST_PASSWORD', 'testpass')

  def setup
    Fluent::Test.setup

    @conn = PG.connect(
      host: HOST,
      user: USERNAME,
      password: PASSWORD
    )
  end

  def teardown
    @conn&.finish
  end

  VALID_CONFIG = %(
    tag postgres.pg_stat_statements
    host #{HOST}
    username #{USERNAME}
    password #{PASSWORD}
    interval 1
  )

  INVALID_CONFIG = %(
    host 'invalid_host.dne'
    port 1234
    username #{USERNAME}
    password #{PASSWORD}
    interval 1
  )

  def create_driver(config)
    Fluent::Test::InputTestDriver.new(Fluent::Plugin::PgStatActivityInput).configure(config)
  end

  sub_test_case 'configuration' do
    test 'connects' do
      d = create_driver(VALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      assert_false emits.empty?
    end

    # Why do we have this test? If postgres is still starting up, we don't want to cause the
    # the fluentd configuration to fail. We would rather retry until we get a connection
    test 'connects for an invalid config' do
      d = create_driver(INVALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      assert_true emits.empty?
    end
  end

  sub_test_case 'execution' do
    test 'connects' do
      d = create_driver(VALID_CONFIG)

      emits = []
      # wait 50 * 0.05, "see fluentd/lib/fluent/test/base.rb:79 num_waits.times { sleep 0.05 }
      d.run(num_waits = 50) do
        emits = d.emits
      end

      first_with_query = emits.find do |event|
        record = event[2]

        record['usename'] == USERNAME &&
          !record['datid'].nil? &&
          !record['query'].nil? &&
          record['state'] == 'active'
      end

      assert_false first_with_query.nil?
      record = first_with_query[2]

      assert_false record['datname'].nil?
      assert_false record['pid'].nil?
      assert_false record['usesysid'].nil?
      assert_false record['application_name'].nil?
      assert_false record['client_addr'].nil?
      assert_false record['client_port'].nil?
      assert_false record['xact_start'].nil?
      assert_false record['xact_age_s'].nil?
      assert_false record['xact_start'].nil?
      assert_false record['xact_age_s'].nil?
      assert_false record['query_start'].nil?
      assert_false record['query_age_s'].nil?
      assert_false record['state_change'].nil?
      assert_false record['state_age_s'].nil?
      assert_false record['query_length'].nil?
      assert_false record['query'].nil?
      assert_false record['fingerprint'].nil?

      assert_true record['xact_age_s'].class == Float
      assert_true record['query_age_s'].class == Float
      assert_true record['state_age_s'].class == Float
      assert_empty record.values.select { |val| val.class == BigDecimal }
    end
  end
end
