# frozen_string_literal: true

require_relative '../helper'

class PgStatStatementsInputTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
  end

  CONFIG = %(
    tag postgres.pg_stat_statements
    host localhost
    port 1234
    dbname gitlab
    sslmode require
    username moo
    password secret
    interval 600
    fingerprint_key fingerprint
  )

  def create_driver
    Fluent::Test::InputTestDriver.new(Fluent::Plugin::PgStatStatementsInput).configure(CONFIG)
  end

  sub_test_case 'configuration' do
    test 'basic configuration' do
      d = create_driver

      assert_equal 'postgres.pg_stat_statements', d.instance.tag
      assert_equal 'localhost', d.instance.host
      assert_equal 1234, d.instance.port
      assert_equal 'gitlab', d.instance.dbname
      assert_equal 'require', d.instance.sslmode
      assert_equal 'moo', d.instance.username
      assert_equal 'secret', d.instance.password
      assert_equal 600, d.instance.interval
      assert_equal 'fingerprint', d.instance.fingerprint_key
    end
  end

  sub_test_case 'execution' do
    test 'sql' do
      d = create_driver
      row = {
        'queryid' => '1234',
        'query' => 'SELECT * FROM users WHERE user_id = 123',
        'calls' => 22,
        'rows' => 333,
        'total_time' => 44.44
      }

      record = d.instance.record_for_row(row)

      expected = {
        'fingerprint' => 'c071dee80d466e7d',
        'query' => 'SELECT * FROM users WHERE user_id = $1',
        'query_length' => 39,
        'queryid' => '1234',
        'calls' => 22,
        'rows' => 333,
        'total_time_ms' => 44.44
      }

      assert_equal expected, record
    end

    test 'nil query' do
      d = create_driver
      row = {
        'queryid' => '1234',
        'query' => nil,
        'calls' => nil,
        'rows' => nil,
        'total_time' => nil
      }
      record = d.instance.record_for_row(row)

      expected = {
        'queryid' => '1234',
        'query' => nil,
        'calls' => nil,
        'rows' => nil,
        'total_time_ms' => nil
      }
      assert_equal expected, record
    end

    test 'ddl query' do
      d = create_driver
      ddl_sql = <<-SQL
        CREATE TABLE accounts (
          user_id serial PRIMARY KEY,
          username VARCHAR(50) UNIQUE NOT NULL,
          password VARCHAR(50) NOT NULL,
          email VARCHAR(255) UNIQUE NOT NULL,
          created_on TIMESTAMP NOT NULL,
          last_login TIMESTAMP
        )
      SQL

      row = {
        'queryid' => 1234,
        'query' => ddl_sql,
        'calls' => 22,
        'rows' => 333,
        'total_time' => 44.44
      }

      record = d.instance.record_for_row(row)

      expected = {
        'fingerprint' => 'fa9c9d26757c4f9b',
        'query' => ddl_sql,
        'query_length' => 287,
        'queryid' => '1234',
        'calls' => 22,
        'rows' => 333,
        'total_time_ms' => 44.44
      }
      assert_equal expected, record
    end

    test 'set command' do
      d = create_driver
      row = {
        'queryid' => 1234,
        'query' => "SET TIME ZONE 'PST8PDT'",
        'calls' => 22,
        'rows' => 333,
        'total_time' => 44.44
      }

      record = d.instance.record_for_row(row)

      expected = {
        'fingerprint' => '23f8d6eb1d3125c3',
        'query' => 'SET TIME ZONE $1',
        'query_length' => 23,
        'queryid' => '1234',
        'calls' => 22,
        'rows' => 333,
        'total_time_ms' => 44.44
      }

      assert_equal expected, record
    end

    test 'unparseable sql' do
      d = create_driver
      record = d.instance.record_for_row({ 'queryid' => 1234, 'query' => 'SELECT * FROM' })

      expected = {
        'query_length' => 13,
        'query_unparseable' => true,
        'query' => 'SELECT * FROM',
        'queryid' => '1234',
        'calls' => nil,
        'rows' => nil,
        'total_time_ms' => nil
      }

      assert_equal expected, record
    end
  end
end
