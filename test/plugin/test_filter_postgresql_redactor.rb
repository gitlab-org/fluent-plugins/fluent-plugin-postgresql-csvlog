# frozen_string_literal: true

require_relative '../helper'

class PostgreSQLRedactorTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
    @tag = 'test.tag'
  end

  CONFIG = ''

  def create_driver(conf = CONFIG)
    Fluent::Test::Driver::Filter.new(Fluent::Plugin::PostgreSQLRedactor).configure(conf)
  end

  test 'filters SQL queries' do
    d = create_driver

    inputs = [
      { 'message' => 'duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects WHERE id = 1',
        'query' => %(SELECT * FROM projects WHERE id = 1),
        'duration_s' => 2.3571 }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(%w[duration_s fingerprint message query_length sql], d.filtered[0].last.keys.sort)
    assert_equal('SELECT * FROM projects WHERE id = $1', d.filtered[0].last['sql'])
  end

  test 'truncates logs' do
    d = create_driver(<<~CONF
      max_length 10
    CONF
                     )

    inputs = [
      { 'message' => 'duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects WHERE id = 1',
        'query' => %(SELECT * FROM projects WHERE id = 1),
        'duration_s' => 2.3571 }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(%w[duration_s fingerprint message query_length sql truncated_query], d.filtered[0].last.keys.sort)
    assert_equal('SELECT * F', d.filtered[0].last['sql'])
  end

  test 'handles parse errors' do
    d = create_driver

    input = { 'query' => 'create index something test (bla) include (bar)' }

    d.run(default_tag: @tag) do
      d.feed(input)
    end

    assert_equal(%w[query query_length query_unparseable], d.filtered[0].last.keys.sort)
    assert_equal(input['query'], d.filtered[0].last['query'])
  end

  test 'uses configured input and output keys' do
    d = create_driver(<<~CONF
      input_key sql
      output_key out_sql
    CONF
                     )

    inputs = [
      {
        'message' => 'duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects WHERE id = 1',
        'sql' => %(SELECT * FROM projects WHERE id = 1),
        'duration_s' => 2.3571
      }
    ]

    d.run(default_tag: @tag) do
      inputs.each { |input| d.feed(input) }
    end

    assert_equal(%w[duration_s fingerprint message out_sql query_length], d.filtered[0].last.keys.sort)
    assert_equal('SELECT * FROM projects WHERE id = $1', d.filtered[0].last['out_sql'])
  end
end
