# frozen_string_literal: true

require_relative '../helper'
require 'date'

class PgStatActivityInputTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
  end

  CONFIG = %(
    tag postgres.pg_stat_activity
    host localhost
    port 1234
    dbname gitlab
    sslmode require
    username moo
    password secret
    interval 600
    fingerprint_key fingerprint
  )

  def create_driver
    Fluent::Test::InputTestDriver.new(Fluent::Plugin::PgStatActivityInput).configure(CONFIG)
  end

  sub_test_case 'configuration' do
    test 'basic configuration' do
      d = create_driver

      assert_equal 'postgres.pg_stat_activity', d.instance.tag
      assert_equal 'localhost', d.instance.host
      assert_equal 1234, d.instance.port
      assert_equal 'gitlab', d.instance.dbname
      assert_equal 'require', d.instance.sslmode
      assert_equal 'moo', d.instance.username
      assert_equal 'secret', d.instance.password
      assert_equal 600, d.instance.interval
      assert_equal 'fingerprint', d.instance.fingerprint_key
    end
  end

  sub_test_case 'execution' do
    test 'sql' do
      d = create_driver
      row = {
        'datid' => 16384,
        'datname' => 'testuser',
        'pid' => 376,
        'usesysid' => 10,
        'usename' => 'testuser',
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'xact_age_s' => 0.001884,
        'query_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'query_age_s' => 0.001894,
        'state_change' => Time.parse('2021-07-23 12:55:25 +0000'),
        'state_age_s' => 0.001894,
        'state' => 'active',
        'query' => "SELECT * FROM users WHERE user_secret = 's3cr3t'"
      }

      record = d.instance.record_for_row(row)

      expected = {
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'datid' => 16384,
        'datname' => 'testuser',
        'fingerprint' => '5c4a61e156c7d822',
        'pid' => 376,
        'query' => 'SELECT * FROM users WHERE user_secret = $1', # NOTE: secret removed
        'query_age_s' => 0.001894,
        'query_length' => 48,
        'query_start' => '2021-07-23T12:55:25.000+00:00',
        'state' => 'active',
        'state_age_s' => 0.001894,
        'state_change' => '2021-07-23T12:55:25.000+00:00',
        'usename' => 'testuser',
        'usesysid' => 10,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_age_s' => 0.001884,
        'xact_start' => '2021-07-23T12:55:25.000+00:00'
      }

      assert_equal expected, record
    end

    # This test mostly checks that the code is nil safe
    test 'nil query' do
      d = create_driver
      record = d.instance.record_for_row({})

      expected = {
        'application_name' => nil,
        'client_addr' => nil,
        'client_hostname' => nil,
        'client_port' => nil,
        'datid' => nil,
        'datname' => nil,
        'pid' => nil,
        'query' => nil,
        'query_age_s' => nil,
        'query_start' => nil,
        'state' => nil,
        'state_age_s' => nil,
        'state_change' => nil,
        'usename' => nil,
        'usesysid' => nil,
        'wait_event' => nil,
        'wait_event_type' => nil,
        'xact_age_s' => nil,
        'xact_start' => nil
      }

      assert_equal expected, record
    end

    test 'unparseable sql' do
      d = create_driver
      row = {
        'datid' => 16384,
        'datname' => 'testuser',
        'pid' => 376,
        'usesysid' => 10,
        'usename' => 'testuser',
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'xact_age_s' => 0.001884,
        'query_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'query_age_s' => 0.001894,
        'state_change' => Time.parse('2021-07-23 12:55:25 +0000'),
        'state_age_s' => 0.001894,
        'state' => 'active',
        'query' => "SELECT * FROM users WHERE user_se="
      }

      record = d.instance.record_for_row(row)

      expected = {
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'datid' => 16384,
        'datname' => 'testuser',
        'pid' => 376,
        'query' => "SELECT * FROM users WHERE user_se=",
        'query_age_s' => 0.001894,
        'query_length' => 34,
        'query_start' => '2021-07-23T12:55:25.000+00:00',
        'query_unparseable' => true,
        'state' => 'active',
        'state_age_s' => 0.001894,
        'state_change' => '2021-07-23T12:55:25.000+00:00',
        'usename' => 'testuser',
        'usesysid' => 10,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_age_s' => 0.001884,
        'xact_start' => '2021-07-23T12:55:25.000+00:00'
      }

      assert_equal expected, record
    end

    test 'marginalia prepended' do
      d = create_driver
      row = {
        'datid' => 16384,
        'datname' => 'testuser',
        'pid' => 376,
        'usesysid' => 10,
        'usename' => 'testuser',
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'xact_age_s' => 0.001884,
        'query_start' => Time.parse('2021-07-23 12:55:25 +0000'),
        'query_age_s' => 0.001894,
        'state_change' => Time.parse('2021-07-23 12:55:25 +0000'),
        'state_age_s' => 0.001894,
        'state' => 'active',
        'query' => "/*application:web,correlation_id:01F1D2T1SC9DM82A4865ATG1CP,endpoint_id:POST /api/:version/groups/:id/-/packages/mavenpath/:file_name*/ SELECT * FROM users WHERE user_secret = 's3cr3t'"
      }

      record = d.instance.record_for_row(row)

      expected = {
        'application' => 'web',
        'application_name' => 'psql',
        'client_addr' => '172.17.0.1',
        'client_hostname' => nil,
        'client_port' => 60182,
        'correlation_id' => '01F1D2T1SC9DM82A4865ATG1CP',
        'datid' => 16384,
        'datname' => 'testuser',
        'endpoint_id' => 'POST /api/:version/groups/:id/-/packages/mavenpath/:file_name',
        'fingerprint' => '5c4a61e156c7d822',
        'pid' => 376,
        'query' => 'SELECT * FROM users WHERE user_secret = $1', # Secret removed
        'query_age_s' => 0.001894,
        'query_length' => 48,
        'query_start' => '2021-07-23T12:55:25.000+00:00',
        'state' => 'active',
        'state_age_s' => 0.001894,
        'state_change' => '2021-07-23T12:55:25.000+00:00',
        'usename' => 'testuser',
        'usesysid' => 10,
        'wait_event' => 'relation',
        'wait_event_type' => 'Lock',
        'xact_age_s' => 0.001884,
        'xact_start' => '2021-07-23T12:55:25.000+00:00'
      }

      assert_equal expected, record
    end
  end
end
