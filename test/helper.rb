# frozen_string_literal: true

require 'bundler/setup'
require 'test/unit'

$LOAD_PATH.unshift(File.join(__dir__, '..', 'lib'))
$LOAD_PATH.unshift(__dir__)
require 'fluent/test'
require 'fluent/test/driver/filter'
require 'fluent/test/driver/input'
require 'fluent/test/helpers'

Test::Unit::TestCase.include(Fluent::Test::Helpers)
Test::Unit::TestCase.extend(Fluent::Test::Helpers)

require 'fluent/plugin/filter_postgresql_slowlog'
require 'fluent/plugin/filter_postgresql_redactor'
require 'fluent/plugin/filter_marginalia'
require 'fluent/plugin/marginalia_extractor'
require 'fluent/plugin/in_pg_stat_statements'
require 'fluent/plugin/in_pg_stat_activity'
