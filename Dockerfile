# Dockerfile useful for manual testing purposes
FROM fluent/fluentd:v1.13-1
USER root
RUN apk add bash alpine-sdk postgresql-dev postgresql-client ruby-dev

WORKDIR /src/

ADD Gemfile /src/
ADD fluent-plugin-postgresql-csvlog.gemspec /src/

ADD . /src/

RUN bundle install
RUN rake build

RUN fluent-gem install pkg/*.gem

ENTRYPOINT [ "/bin/bash"]
