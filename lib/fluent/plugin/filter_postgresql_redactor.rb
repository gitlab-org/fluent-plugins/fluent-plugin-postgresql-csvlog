# frozen_string_literal: true

require 'fluent/plugin/filter'

require_relative './query_normalizer'

module Fluent::Plugin
  class PostgreSQLRedactor < Filter
    Fluent::Plugin.register_filter('postgresql_redactor', self)

    desc 'Input field to parse for SQL queries'
    config_param :input_key, :string, default: 'query'

    desc 'Output field to store SQL queries'
    config_param :output_key, :string, default: 'sql'

    desc 'Name of field to store SQL query fingerprint'
    config_param :fingerprint_key, :string, default: 'fingerprint'

    desc 'Truncate the query if it exceeds the number of bytes'
    config_param :max_length, :integer, default: 3 * 1024 * 1024

    include QueryNormalizer

    def filter(_tag, _time, record)
      opts = {
        input_key: @input_key,
        output_key: @output_key,
        fingerprint_key: @fingerprint_key,
        max_length: @max_length
      }

      normalize_and_fingerprint_query(record, opts)
    end
  end
end
