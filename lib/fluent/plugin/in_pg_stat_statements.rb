# frozen_string_literal: true

require_relative './polling_pg_input_plugin'
require_relative './query_normalizer'

module Fluent::Plugin
  # PgStatStatementsInput will periodically poll postgres, querying pg_stat_statements
  # for queryid to query mappings. These are then normalized for security purposes
  # fingerprinted and emitted as records with the following format:
  # {
  #  'fingerprint' => '8a6e9896bd9048a2',
  #  'query' => 'SELECT * FROM table ORDER BY queryid LIMIT $1',
  #  'query_length' => 58,
  #  'queryid' => '3239318621761098074'
  # }
  class PgStatStatementsInput < PollingPostgresInputPlugin
    Fluent::Plugin.register_input('pg_stat_statements', self)

    desc 'Name of field to store SQL query fingerprint'
    config_param :fingerprint_key, :string, default: 'fingerprint'

    desc 'Truncate the query if it exceeds the number of bytes'
    config_param :max_length, :integer, default: 3 * 1024 * 1024

    include QueryNormalizer

    POSTGRES_SERVER_VERSION_QUERY = "SELECT current_setting('server_version_num')"

    PG12_STAT_STATEMENTS_QUERY = <<-SQL
      SELECT queryid,
             query,
             calls,
             rows,
             total_time
        FROM public.pg_stat_statements
    SQL

    PG13_STAT_STATEMENTS_QUERY = <<-SQL
      SELECT queryid,
             query,
             calls,
             rows,
             (total_plan_time + total_exec_time) total_time
        FROM public.pg_stat_statements
    SQL

    protected

    def on_poll
      with_connection do |conn|
        emit_statements_to_stream(conn)
      end
    end

    public

    def initialize
      super
      @postgres_server_version_num = nil
    end

    # Returns a fluentd record for a query row
    def record_for_row(row)
      record = {
        'query' => row['query'],
        'queryid' => row['queryid'].to_s,
        'calls' => row['calls']&.to_i,
        'total_time_ms' => row['total_time']&.to_f,
        'rows' => row['rows']&.to_i
      }

      opts = {
        input_key: 'query',
        output_key: 'query',
        fingerprint_key: @fingerprint_key,
        max_length: @max_length
      }

      normalize_and_fingerprint_query(record, opts)
    end

    # Query the database and emit statements to fluentd router
    def emit_statements_to_stream(conn)
      me = Fluent::MultiEventStream.new

      now = Fluent::Engine.now

      query = query_for_postgres_version(conn)
      records = conn.exec(query).to_a

      records.each do |row|
        record = record_for_row(row)
        me.add(now, record)
      end

      @router.emit_stream(@tag, me)
    end

    # Returns the PG_VERSION_NUM value from the database
    # will memoize the result
    def postgres_server_version_num(conn)
      return @postgres_server_version_num if @postgres_server_version_num

      @postgres_server_version_num = conn.exec(POSTGRES_SERVER_VERSION_QUERY).getvalue(0,0).to_i
    end

    # pg_stat_statements columns changed in pg13, so we use different queries depending on the version
    # https://www.postgresql.org/docs/12/pgstatstatements.html
    # https://www.postgresql.org/docs/13/pgstatstatements.html
    def query_for_postgres_version(conn)
      return PG13_STAT_STATEMENTS_QUERY if postgres_server_version_num(conn) >= 13_00_00

      PG12_STAT_STATEMENTS_QUERY
    end
  end
end
