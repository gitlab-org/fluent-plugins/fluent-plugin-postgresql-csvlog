# frozen_string_literal: true

require 'pg_query'

module Fluent::Plugin
  module QueryNormalizer
    def normalize_and_fingerprint_query(record, opts)
      input_key = opts[:input_key]
      output_key = opts[:output_key]
      fingerprint_key = opts[:fingerprint_key]
      max_length = opts[:max_length]
      statement = record[input_key]

      return record unless statement

      # We record the query_length as it will help in understanding whether unparseable
      # queries are truncated.
      record['query_length'] = statement&.length
      normalized = PgQuery.normalize(statement)

      record.delete(input_key)
      record[fingerprint_key] = PgQuery.fingerprint(normalized) if fingerprint_key
      record[output_key] = truncate_query(record, normalized, max_length)

      record
    rescue PgQuery::ParseError
      record['query_unparseable'] = true
      record
    end

    private

    def truncate_query(record, normalized, max_length)
      return normalized if normalized.length < max_length

      record['truncated_query'] = true
      # Assume UTF-8 encoding for PostgreSQL queries
      utf8_query = normalized.encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
      utf8_query[0, max_length]
    end
  end
end
