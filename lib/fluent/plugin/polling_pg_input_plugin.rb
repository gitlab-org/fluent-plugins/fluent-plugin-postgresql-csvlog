# frozen_string_literal: true

require 'fluent/plugin/input'
require 'pg'
require 'pg_query'

module Fluent::Plugin
  # PollingPostgresInputPlugin is intended to be used as an base class
  # for input plugins that poll postgres.
  #
  # Child classes should implement the `on_poll` method
  class PollingPostgresInputPlugin < Input
    desc 'PostgreSQL host'
    config_param :host, :string

    desc 'RDBMS port (default: 5432)'
    config_param :port, :integer, default: 5432

    desc 'login user name'
    config_param :username, :string, default: nil

    desc 'postgres db'
    config_param :dbname, :string, default: nil

    desc 'login password'
    config_param :password, :string, default: nil, secret: true

    # See https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNECT-SSLMODE
    # for options
    desc 'postgres sslmode'
    config_param :sslmode, :string, default: 'prefer'

    desc 'tag'
    config_param :tag, :string, default: nil

    desc 'interval in second to run query'
    config_param :interval, :time, default: 300

    def start
      @stop_flag = false
      @thread = Thread.new(&method(:thread_main))
    end

    # Fluentd shutdown method, called to terminate and cleanup plugin
    def shutdown
      @stop_flag = true

      # Interrupt thread and wait for it to finish
      Thread.new { @thread.run } if @thread
      @thread.join
    end

    # Main polling loop on thread
    def thread_main
      until @stop_flag
        sleep @interval
        break if @stop_flag

        begin
          on_poll
        rescue StandardError => e
          log.error 'unexpected error', error: e.message, error_class: e.class
          log.error_backtrace
        end
      end
    end

    protected

    # Child-classes should implement this method
    def on_poll
      raise 'on_poll must be implemented by descendents of PollingPostgresInputPlugin'
    end

    # Since this query is very infrequent, and it may be communicating directly
    # with postgres without pgbouncer, don't use a persistent connection and
    # ensure that it is properly closed
    def with_connection(&block)
      conn = PG.connect(
        host: @host,
        dbname: @dbname,
        sslmode: @sslmode,
        user: @username,
        password: @password
      )

      map = PG::BasicTypeMapForResults.new(conn)
      map.default_type_map = PG::TypeMapAllStrings.new

      conn.type_map_for_results = map

      begin
        block.call(conn)
      ensure
        # Always close the connection
        conn.finish
      end
    end
  end
end
