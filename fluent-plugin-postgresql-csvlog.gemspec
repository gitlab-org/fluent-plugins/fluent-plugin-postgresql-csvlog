$:.push File.expand_path('lib', __dir__)

Gem::Specification.new do |s|
  s.name        = 'fluent-plugin-postgresql-csvlog'
  s.version     = '0.11.0'
  s.authors     = ['stanhu']
  s.email       = ['stanhu@gmail.com']
  s.homepage    = 'https://gitlab.com/gitlab-org/fluent-plugins/fluent-plugin-postgresql-csvlog'
  s.summary     = 'fluentd plugins to work with PostgreSQL CSV logs'
  s.description = 'fluentd plugins to work with PostgreSQL CSV logs'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map { |f| File.basename(f) }
  s.require_paths = ['lib']

  s.add_dependency 'fluentd', ['>= 1.0', '< 2']
  s.add_dependency 'pg', '~> 1.5'
  s.add_dependency 'pg_query', '~> 6.0'

  s.add_development_dependency 'rake'
  s.add_development_dependency 'test-unit', '~> 3.2'
end
